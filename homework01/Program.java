import java.util.Scanner;

class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int result = 9;
        while (number != -1) {
            int smallest = number % 10;
            while (number > 0) {
                int reminder = number % 10;
                if (smallest > reminder) {
                    smallest = reminder;
                }
                number = number / 10;
            }
            if (smallest < result) {
                result = smallest;
            }
            number = scanner.nextInt();
        }
        System.out.println("Ответ: " + result);
    }
}
